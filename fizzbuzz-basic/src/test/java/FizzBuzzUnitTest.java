import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzUnitTest {

    @Test
    public void testConvertMultipleOfThree() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Fizz", fizzBuzz.convert(3));
    }

    @Test
    public void testConvertMultipleOfFive() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Buzz", fizzBuzz.convert(5));
    }

    @Test
    public void testConvertMultipleOfThreeAndFive() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("FizzBuzz", fizzBuzz.convert(15));
    }

    @Test
    public void testConvertNoMultiple() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("14", fizzBuzz.convert(14));
    }
}
