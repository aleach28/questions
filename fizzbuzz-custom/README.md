# Questions
## 4
To support multiple games, I would implement a simple `RuleEngine` that could be configured with one or many `Rule` objects. This would make use of an abstract principle and allow the engine to be polymorphic regarding what rules (if any) it applies to the input number. 

For example, a rule might have the following `MultipleOfRule` implementation:

```java
public class MultipleOfRule implements Rule {  
  
    private final int multiple;  
    private final String word;  
  
    public MultipleOfRule(int multiple, String word) {  
       this.multiple = multiple;  
       this.word = word;  
    }  
  
    @Override
    public String apply(int input) {  
        if (input % multiple == 0) {  
            return word;  
        }  
  
        return "";  
    }  
}
```
Rules would be implemented by extending and implementing the `Rule` interface. The application would iterate through each rule and manipulate the value.

## 5
There would be a UI where the user could define the game parameters.

As a CLI, the following usage could be provided:

`usage: input [rule_options]`

**input:** is the input number
**rule_options:** is an optional list defining the rule name, criteria and the word to output if the rule is applied.

Therefore, given the following example arguments:

`15 MULTIPLE:3:Fizz MULTIPLE:5:Buzz EQUALITY:4:Pop EQUALITY:15:Bozz`

...the application would return `FizzBuzzBozz`.


