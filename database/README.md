
# Database Questions

## Assumptions

1) A door can have multiple access codes.

2) A user could have multiple access codes for the same door, each with their own set of permissions.

3) A door can't have access codes with the same code numbers (eg 1234). A unique constraint on door_id and number prevents this.

4) A user can use the same code number for many different doors, assuming that number doesn't already exist in a different access code for that door.

5) A new access code and permissions entity are created for each door a user is assigned to.


## Notes

1) We can traverse across all tables from the AccessCode entity. By utilising lazy loading there's minimal performance impact when compared to performing several lookups, so long as the data isn't huge.

2) Surrogate keys are used for identifiers.

![Structure](structure.PNG)

## Queries




```sql
1) 
SELECT
   d.id 
FROM
   door d 
   INNER JOIN
      accesscode ac 
      ON d.id = ac.door_id 
   INNER JOIN
      user u 
      ON u.id = ac.user_id 
WHERE
   u.first = 'Alex';


2)
SELECT
   b.id 
FROM
   building b 
   INNER JOIN
      door d 
      ON b.id = d.building_id 
   INNER JOIN
      accesscode ac 
      ON d.id = ac.door_id 
   INNER JOIN
      user u 
      ON u.id = ac.user_id 
WHERE
   u.first = 'Rupert';


3)
SELECT
   COUNT(DISTINCT(user_id)) 
FROM
   accesscode ac 
GROUP BY
   ac.door_id;


4)
UPDATE
   doorpermissions AS dp 
   INNER JOIN
      accesscode ac 
      ON dp.accesscode_id = ac.id
SET
   dp.enabled = 0 
WHERE
   ac.user_id = '1';


--- OR ---


DELETE
   dp 
FROM
   doorpermissions AS dp 
   INNER JOIN
      accesscode ac 
      ON dp.accesscode_id = ac.id 
WHERE
   ac.user_id = '1';


DELETE
   ac 
FROM
   accesscode AS ac 
WHERE
   ac.user_id = '1';
```

